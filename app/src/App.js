import logo from './logo.png';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          OK Baff, no fair making me learn Web stuffs.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Spurn React
        </a>
		<p>🔴red 🔷blue 💛yellow 📗green</p>
      </header>
    </div>
  );
}

export default App;
