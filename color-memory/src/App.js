import './App.css';

import * as React from "react";
// import Form from "./Form";

// <https://usehooks.com/usehistorystate>
// <https://react.dev/learn/managing-state>
import { useHistoryState } from "@uidotdev/usehooks";

const kRedText = "🔴 red";
const kBlueText = "🔷 blue";
const kYellowText = "💛 yellow";
const kGreenText = "📗 green";

export default function App() {
  const { state, set, undo, redo, clear, canUndo, canRedo } = useHistoryState({
    items: [],
  });

  const addText = (val) => {
    set({
      ...state,
      items: state.items.concat({ id: Date.now(), text: val }),
    });
  };

//   function Button({ addItemFunc, children }) {
//     return (
//       <button onClick={addItemFunc}>
//         {children}
//       </button>
//     );
//   }

  function ColorButton({ colorText }) {
//     function handleColorClick() {
//       addText({colorText});
//     }

    return (
      <button onClick={() => addText(colorText)}>
        {colorText}
      </button>
    );
  }

  return (
    <div className="App">
        <h1>
          color memory game remix
        </h1>
{
// 		<p>🔴red 🔷blue 💛yellow 📗green</p>
// 		<p>🔴red 🔷blue 💛yellow 📗green</p>
// 		<p>🔴red 🔷blue 💛yellow 📗green</p>
// 		<p>🔴red 🔷blue 💛yellow 📗green</p>
}
        <div>
          <button disabled={!canUndo} className="link" onClick={undo}>
            Undo
          </button>
          <button disabled={!canRedo} className="link" onClick={redo}>
            Redo
          </button>

          <button
            disabled={!state.items.length}
            className="link"
            onClick={clear}
          >
            Clear
          </button>

          <ColorButton colorText={kRedText} />
          <ColorButton colorText={kBlueText} />
          <ColorButton colorText={kYellowText} />
          <ColorButton colorText={kGreenText} />
        </div>

      <ul class="no-bullet">
{
//             <li key="0">
//               <span>butt</span>
//             </li>
}
       {state.items.map((item, index) => {
          return (
            <li key={index}>
              <span>{item.text}</span>
            </li>
          );
        })}
      </ul>
    </div>
  );
}
